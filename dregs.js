class REPart {
    derive (char) { return NeverMatches; }
    withNext(next) {
        let copy = Object.assign({}, this);
        copy.next = next;
        return copy;
    }
}

class _EmptyString extends REPart {}
export const EmptyString = new _EmptyString();
class _NeverMatches extends REPart {}
export const NeverMatches = new _NeverMatches();

export class REChar extends REPart {
    constructor (char, next) {
        super();
        this.char = char;
        this.next = next;
    }

    derive (char) {
        if (char === this.char) {
            return this.next;
        } else {
            return NeverMatches;
        }
    }
}

export class REAlternation extends REPart {
    constructor (alternatives) {
        super();
        let _alternatives = alternatives.filter((alt) => alt !== NeverMatches);
        if (_alternatives.length === 0) {
            return NeverMatches;
        } else if (_alternatives.length === 1) {
            return _alternatives[0];
        } else {
            this.alternatives = _alternatives;
        }
        return this;
    }

    derive (char) {
        return new REAlternation(this.alternatives.map((alt) => alt.derive(char)));
    }
}

export class REAtLeastZero extends REPart {
    constructor (repeatable, next) {
        super();
        this.repeatable = repeatable;
        this.next = next;
    }

    derive (char) {
        return new REAlternation([
            this.repeatable.derive(char),
            this.repeatable
        ], this.next);
    }
}

export class RE {
    constructor(src, options={}) {
    }
}

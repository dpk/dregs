# dregs

> Is that a time this Great work to begin?  
> Shall _Satan_ be preſented with the prime  
> and _Wisdom_ only have the Dregs of Time?

— _Youths tragedy, a poem_ (Thomas Sherman, 1672)

> \<dpk> i have a good name for my derivative-based regex engine. dregs!  
> \<cajg> scraping the barrel there, dpk  
> \<dpk> well, i thought it was a good name anyway

— David P. Kendal and Charles Goodier (2017)
